import sys
from PySide6.QtWidgets import QApplication
from Widgets.Appointment import Appointment

'''
Entry point to the desktop version of doctor-appointment
'''
if __name__ == '__main__':
    app = QApplication()
    main = Appointment()
    sys.exit(app.exec())