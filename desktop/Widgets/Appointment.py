import requests
from PySide6.QtWidgets import QWidget, QLabel, QLineEdit, QVBoxLayout, QGridLayout, QComboBox, QPushButton, QMessageBox

class Appointment(QWidget):

    def __init__(self) -> None:
        super().__init__()
        self.initComponents()
        self.initLayout()
        self.show()
    
    '''
    initialize all components for this window
    '''
    def initComponents(self):
        # data from the database
        self.patients: list[dict] = []
        self.actualPatientIndex = 0

        # data for the database
        self.appointment: dict = {}
        self.appointment.update({'patient_id':''})
        self.appointment.update({'doctor':''})
        self.appointment.update({'date':''})
        self.appointment.update({'time':''})
        self.appointment.update({'confirmed':'0'})

        self.labellist: list[QLabel] = []
        self.patientdatalist: list[QLineEdit] = []
        self.confirm: QComboBox = QComboBox()
        self.confirm.addItem('Nein')
        self.confirm.addItem('Ja')
        self.confirm.setCurrentIndex(0)
        self.loadBtn = QPushButton('Terminanfragen')
        self.okBtn = QPushButton('Termin vergeben')
        self.okBtn.setEnabled(False)
        self.leftBtn = QPushButton('<')
        self.leftBtn.setEnabled(False)
        self.rightBtn = QPushButton('>')
        self.rightBtn.setEnabled(False)
        self.deleteBtn = QPushButton('Anfrage entfernen')
        self.deleteBtn.setEnabled(False)

        for _ in range(12):
            self.labellist.append(QLabel())
        for n in range(11):
            self.patientdatalist.append(QLineEdit())
            if n <= 7:
                self.patientdatalist[n].setEnabled(False)
            
        self.labellist[0] = QLabel("Vorname")
        self.labellist[1] = QLabel("Nachname")
        self.labellist[2] = QLabel("Straße")
        self.labellist[3] = QLabel("Hausnummer")
        self.labellist[4] = QLabel("PLZ")
        self.labellist[5] = QLabel("Stadt")
        self.labellist[6] = QLabel("Telefon")
        self.labellist[7] = QLabel("Email")

        self.labellist[8] = QLabel("Doctor")
        self.labellist[9] = QLabel("Tag")
        self.labellist[10] = QLabel("Uhrzeit")
        self.labellist[11] = QLabel("Bestätigen")

        self.confirm.currentTextChanged.connect(self.editConfirmed)
        self.patientdatalist[8].textEdited.connect(self.editDoctor)
        self.patientdatalist[9].textEdited.connect(self.editDate)
        self.patientdatalist[10].textEdited.connect(self.editTime)
        self.loadBtn.clicked.connect(self.loadAppointmentRequests)
        self.okBtn.clicked.connect(self.sendDataToServer)
        self.leftBtn.clicked.connect(self.decPatientIndex)
        self.rightBtn.clicked.connect(self.incPatientIndex)
        self.deleteBtn.clicked.connect(self.deleteAppointment)

    '''
    the window layout
    '''
    def initLayout(self):
        box: QVBoxLayout = QVBoxLayout()
        grid: QGridLayout = QGridLayout()
        
        grid.addWidget(self.labellist[0], 0, 0)
        grid.addWidget(self.patientdatalist[0], 0, 1)
        grid.addWidget(self.labellist[1], 0, 2)
        grid.addWidget(self.patientdatalist[1], 0, 3)
        grid.addWidget(self.labellist[2], 1, 0)
        grid.addWidget(self.patientdatalist[2], 1, 1)
        grid.addWidget(self.labellist[3], 1, 2)
        grid.addWidget(self.patientdatalist[3], 1, 3)
        grid.addWidget(self.labellist[4], 2, 0)
        grid.addWidget(self.patientdatalist[4], 2, 1)
        grid.addWidget(self.labellist[5], 2, 2)
        grid.addWidget(self.patientdatalist[5], 2, 3)
        grid.addWidget(self.labellist[6], 3, 0)
        grid.addWidget(self.patientdatalist[6], 3, 1)
        grid.addWidget(self.labellist[7], 3, 2)
        grid.addWidget(self.patientdatalist[7], 3, 3)

        grid.addWidget(self.labellist[8], 4, 0)
        grid.addWidget(self.patientdatalist[8], 4, 1)
        grid.addWidget(self.labellist[9], 4, 2)
        grid.addWidget(self.patientdatalist[9], 4, 3)
        grid.addWidget(self.labellist[10], 5, 0)
        grid.addWidget(self.patientdatalist[10], 5, 1)
        grid.addWidget(self.labellist[11], 5, 2)
        grid.addWidget(self.confirm, 5, 3)

        grid.addWidget(self.leftBtn, 6, 0)
        grid.addWidget(self.loadBtn, 6, 1)
        grid.addWidget(self.rightBtn, 6, 2)
        grid.addWidget(self.okBtn, 6, 3)
        grid.addWidget(self.deleteBtn, 7, 3)

        box.addLayout(grid)
        self.setLayout(box)

    
    '''
    update patient data for the gui
    '''
    def showPatientData(self):
        self.patientdatalist[0].setText(self.patients[self.actualPatientIndex].get('forname'))
        self.patientdatalist[1].setText(self.patients[self.actualPatientIndex].get('surname'))
        self.patientdatalist[2].setText(self.patients[self.actualPatientIndex].get('street'))
        self.patientdatalist[3].setText(self.patients[self.actualPatientIndex].get('housenumber'))
        self.patientdatalist[4].setText(self.patients[self.actualPatientIndex].get('postalcode'))
        self.patientdatalist[5].setText(self.patients[self.actualPatientIndex].get('city'))
        self.patientdatalist[6].setText(self.patients[self.actualPatientIndex].get('phonenumber'))
        self.patientdatalist[7].setText(self.patients[self.actualPatientIndex].get('email'))
        self.patientdatalist[8].setText(self.patients[self.actualPatientIndex].get('doctor'))
        self.patientdatalist[9].setText(self.patients[self.actualPatientIndex].get('date'))
        self.patientdatalist[10].setText(self.patients[self.actualPatientIndex].get('time'))
        if self.patients[self.actualPatientIndex].get('confirmed') == 0:
            self.confirm.setCurrentIndex(0)
        else:
            self.confirm.setCurrentIndex(1)    

    '''
    GET request to the server to obtain patient data
    '''
    def loadAppointmentRequests(self):
        self.patients = []
        self.actualPatientIndex = 0
        try:
            data = requests.get('http://localhost:8080/api/allPatients')
            patients = data.json()
            for elem in patients:
                temp: dict = {'patient_id': elem.get('id'), 'forname': elem.get('forname'), 'surname': elem.get('surname'), 'street': elem.get('street'), 'housenumber': elem.get('housenumber'), 'postalcode': elem.get('postalcode'), 'city': elem.get('city'), 'phonenumber': elem.get('phonenumber'), 'email': elem.get('email'), 'doctor': elem.get('doctor'), 'date': elem.get('date'), 'time': elem.get('time'), 'confirmed': elem.get('confirmed')}
                self.patients.append(temp)
            if len(self.patients) > 0:
                self.leftBtn.setEnabled(True)
                self.rightBtn.setEnabled(True)
                self.okBtn.setEnabled(True)
                self.deleteBtn.setEnabled(True)
                self.showPatientData()
            else:
                self.leftBtn.setEnabled(False)
                self.rightBtn.setEnabled(False)
                self.okBtn.setEnabled(False)
                self.deleteBtn.setEnabled(False)
        except Exception:
            dialog: QMessageBox = QMessageBox(self)
            dialog.setWindowTitle('Fehler')
            dialog.setText('Fehler beim empfangen von Patientendaten. Server konnte nicht erreicht werden.')
            dialog.exec()

    '''
    send updated patient data to the server
    '''
    def sendDataToServer(self):
        self.appointment['patient_id'] = self.patients[self.actualPatientIndex].get('patient_id')
        try:
            requests.patch('http://localhost:8080/api/confirmAppointment', json=self.appointment)
            dialog: QMessageBox = QMessageBox(self)
            dialog.setWindowTitle('Erfolgreich')
            dialog.setText('Daten wurden dem Server übermittelt.')
            dialog.exec()
        except Exception:
            dialog: QMessageBox = QMessageBox(self)
            dialog.setWindowTitle('Fehler')
            dialog.setText('Fehler beim senden von Patientendaten. Server konnte nicht erreicht werden.')
            dialog.exec()


    def decPatientIndex(self):
        if self.actualPatientIndex == 0:
            return
        self.actualPatientIndex -= 1
        self.showPatientData()


    def incPatientIndex(self):
        if self.actualPatientIndex == len(self.patients) - 1:
            return
        self.actualPatientIndex += 1
        self.showPatientData()


    '''
    make a delete request to the server and remove patient
    '''
    def deleteAppointment(self):
        data = {'patient_id': self.patients[self.actualPatientIndex].get('patient_id')}
        try:
            requests.delete('http://localhost:8080/api/deletePatient/' + str(data.get('patient_id')), json=data)
            self.patients.pop(self.actualPatientIndex)
            self.actualPatientIndex = 0
            self.showPatientData()
            dialog: QMessageBox = QMessageBox(self)
            dialog.setWindowTitle('Löschen')
            dialog.setText('Terminanfrage wurde entfernt.')
            dialog.exec()
        except Exception:
            dialog: QMessageBox = QMessageBox(self)
            dialog.setWindowTitle('Fehler')
            dialog.setText('Fehler beim senden von Patientendaten. Server konnte nicht erreicht werden.')
            dialog.exec()


    '''
    update flag if an appointment is confirmed
    '''
    def editConfirmed(self, text: str):
        if text == 'Ja':
            self.appointment['confirmed'] = '1'
        else:
            self.appointment['confirmed'] = '0'


    def editDoctor(self, text: str):
        self.appointment['doctor'] = text


    def editDate(self, text: str):
        self.appointment['date'] = text


    def editTime(self, text: str):
        self.appointment['time'] = text
