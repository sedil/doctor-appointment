import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import appointmentReducer from '../features/appointmentform/AppointmentSlice';
import overviewReducer from '../features/overview/OverviewSlice';

export const store = configureStore({
  reducer: {
    contact: appointmentReducer,
    overview: overviewReducer
  }
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
