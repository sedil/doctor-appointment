import { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { getAppointmentsFromServer, handleInput } from "./OverviewSlice";
import './Overview.css';
import Contact from "../contact/Contact";

const Overview = () => {
    /* all patient appointments */
    const appointments = useAppSelector(state => state.overview.appointmentlist);
    /* for the patient filter */
    const patient = useAppSelector(state => state.overview.patient);
    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getAppointmentsFromServer());
    }, []);

    return <>
    <div id='appointmentlist'>
        {appointments.map((elem, index) => elem.confirmed && elem.forname == patient.forname && elem.surname == patient.surname ? 
        <div key={index} id='appointmentlist_entry'>
            <p>{index+1}</p>
            <p>{elem.doctor}</p>
            <p>{elem.date}</p>
            <p>{elem.time}</p> 
        </div> : null)}
    </div>
    <div id='filtercontainer'>
        <div>
            <label className="label">Vorname</label>
            <input className="filterinput" type="text" name="forname" value={patient.forname} onChange={e => dispatch(handleInput(e))}></input>
        </div>
        <div>
            <label className="label">Nachname</label>
            <input className="filterinput" type="text" name="surname" value={patient.surname} onChange={e => dispatch(handleInput(e))}></input>
        </div>
    </div>
    <Contact />
    </>
};

export default Overview;