import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export interface Appointment {
    forname: string,
    surname: string,
    doctor: string,
    date: string,
    time: string,
    confirmed: boolean
}

export interface Patient {
    forname : string,
    surname : string
}

interface Appointments {
    patient: Patient,
    appointmentlist: Appointment[]
}

const initialState: Appointments = {
    patient: {
        forname : "",
        surname : ""
    },
    appointmentlist : []
};

export const OverviewSlice = createSlice({
    name: 'overview',
    initialState,
    reducers: {
        handleInput : (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement>>) => {
            if(action.payload.target.name === 'forname'){
                state.patient.forname = action.payload.target.value;
            } else if(action.payload.target.name === 'surname'){
                state.patient.surname = action.payload.target.value;
            }
        }
    },
    /* fetch appointments from the server */
    extraReducers: (builder) => {
        builder.addCase(getAppointmentsFromServer.fulfilled, (state, action) => {
            state.appointmentlist = action.payload;
        });
        builder.addCase(getAppointmentsFromServer.rejected, () => {
            //window.alert("Konnte Termine nicht vom Server laden.");
        });
        builder.addCase(getAppointmentsFromServer.pending, () => {
            //window.alert("Konnte Termine nicht vom Server laden.");
        });
    }
});

export const getAppointmentsFromServer = createAsyncThunk("schedules", async() => {
    const response = await fetch("http://localhost:8080/api/appointments", {
        method: 'GET',
        headers : {'Content-Type':'application/json'}
    });
    return response.json();
});

export const { handleInput } = OverviewSlice.actions;

export default OverviewSlice.reducer;