import './Contact.css';

/* static informations about the doctor */
const Contact = () => {
    return <div id='contact_container'>
        <div id='address'>
            <p className='contactparagraph'>Gemeinschaftspraxis Dr. Wendel und Dr. Orno</p>
            <p className='contactparagraph'>Fliesenstrasse 3</p>
            <p className='contactparagraph'>21156 Hirthausen</p>
            <p className='contactparagraph'>Tel. +49 1443 51220</p>
            <p className='contactparagraph'>Email: praxis_kontakt@arztkreis.de</p>
            <p className='contactparagraph'>Website: www.gemeinschaftspraxis-wendel-orno.de</p>
        </div>
    </div>
}

export default Contact;