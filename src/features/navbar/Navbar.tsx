import { Link } from 'react-router-dom';
import './Navbar.css';

interface NavbarProps {
    routes: string[]
}

/* navbar to switch between make an appointment and list all appointments */
const Navbar = (props: NavbarProps) => {
    const navbartext: string[] = ['Termin vereinbaren','Meine Termine'];
    return <nav id='navbar'>
        <ul>
            <Link to={props.routes[0]} key={navbartext[0]}>{navbartext[0]}</Link>
            <Link to={props.routes[1]} key={navbartext[1]}>{navbartext[1]}</Link>
        </ul>
    </nav>
}

export default Navbar;