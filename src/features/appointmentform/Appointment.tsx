import React from "react";
import { useAppSelector, useAppDispatch } from "../../app/hooks";
import { handleFormInput, isInputValid } from "./AppointmentSlice";
import Contact from "../contact/Contact";
import './Appointment.css';

const Appointmentform = () => {
    /* get patient state */
    const patientdata = useAppSelector(state => state.contact);
    const dispatch = useAppDispatch();
    return <><form>
        <div id='formcontainer'>
            <div id='postal'>
                <div className='inputcontainer'>
                    <label>Vorname</label>
                    <input name='forname' type="text" value={patientdata.forname} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Nachname</label>
                    <input name='surname' type="text" value={patientdata.surname} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Straße</label>
                    <input name='street' type="text" value={patientdata.street} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Hausnummer</label>
                    <input name='housenumber' type="text" value={patientdata.housenumber} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Postleitzahl</label>
                    <input name='postalcode' type="text" value={patientdata.postalcode} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Ort</label>
                    <input name='city' type="text" value={patientdata.city} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
            </div>
            <div id='contact'>
                <div className='inputcontainer'>
                    <label>Telefonnummer</label>
                    <input name='phonenumber' type="text" value={patientdata.phonenumber} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div className='inputcontainer'>
                    <label>Email*</label>
                    <input name='email' type="email" value={patientdata.email} onChange={e => dispatch(handleFormInput(e))}></input>
                </div>
                <div id='optional_paragraph'>
                    <p>Angaben in den Feldern mit einem Stern sind optional</p>
                </div>
                <div id='button_container'>
                    <button id="submit_button" onClick={e => dispatch(isInputValid(e))}>Termin vereinbaren</button>
                </div>
            </div>
        </div>
    </form>
    <Contact />
    </>
}

export default Appointmentform;