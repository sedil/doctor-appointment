import { PayloadAction, createSlice } from "@reduxjs/toolkit";

/* type for the Appointment */
export interface AppointmentState {
    forname : string,
    surname : string,
    street : string,
    housenumber : string,
    postalcode : string,
    city : string,
    phonenumber : string,
    email : string
}

const initialState : AppointmentState = {
    forname : "",
    surname : "",
    street : "",
    housenumber : "",
    postalcode : "",
    city : "",
    phonenumber : "",
    email : ""
}

export const AppointmentSlice = createSlice({
    name : 'appointment',
    initialState,
    reducers : {
        /* check if there is no empty field except for email */
        isInputValid : (state, action: PayloadAction<React.MouseEvent<HTMLButtonElement>>) => {
            action.payload.preventDefault();
            console.log(action.payload);
            for(let [key, value] of Object.entries(state)){
                if(key !== 'email'){
                    if(value.length === 0){
                        window.alert("leeres Feld bei "+key);
                        return;
                    }
                }
            }
            sendToServer(state);
            
        },
        /* update the appointment state from the form input */
        handleFormInput : (state, action: PayloadAction<React.ChangeEvent<HTMLInputElement>>) => {
            switch(action.payload.target.name){
                case "forname":
                    state.forname = action.payload.target.value;
                    break;
                case "surname":
                    state.surname = action.payload.target.value;
                    break;
                case "street":
                    state.street = action.payload.target.value;
                    break;
                case "housenumber":
                    state.housenumber = action.payload.target.value;
                    break;
                case "postalcode":
                    state.postalcode = action.payload.target.value;
                    break;
                case "city":
                    state.city = action.payload.target.value;
                    break;
                case "phonenumber":
                    state.phonenumber = action.payload.target.value;
                    break;
                case "email":
                    state.email = action.payload.target.value;
                    break;
                default:
                    return;
            }
        }
    },
    extraReducers: (builder) => {}
});

/* send patient data to the server */
const sendToServer = (appointment: AppointmentState) => {
    fetch('http://localhost:8080/api/appointmentRequest', {
        method: 'POST',
        headers: {'Content-Type' : 'application/json'},
        body: JSON.stringify(appointment)
    }).then(
        response => {
            if(!response.ok){
                window.alert("Es gab ein Fehler");
            } else {
                window.alert("Terminanfrage wurde versendet");
            }
        }
    ).catch(
        () => {
            window.alert("Fehler");
        }
    )
}

export const { handleFormInput, isInputValid } = AppointmentSlice.actions;

export default AppointmentSlice.reducer;