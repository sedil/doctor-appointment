import React from 'react';
import Appointment from './features/appointmentform/Appointment';
import Overview from './features/overview/Overview';
import Navbar from './features/navbar/Navbar';
import { Route, Routes, BrowserRouter} from 'react-router-dom';

function App() {
  const routes: string[] = ['/contactform','/myappointments'];
  return <BrowserRouter>
  <Navbar routes={routes}/>
  <Routes>
    <Route path='/' element={<Appointment />}></Route>
    <Route path='/*' element={<Appointment />}></Route>
    <Route path='/contactform' element={<Appointment />}></Route>
    <Route path='/myappointments' element={<Overview />}></Route>
  </Routes>
  </BrowserRouter>
}

export default App;
