package com.app.doctorappointment.appointment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api")
@CrossOrigin("*")
public class AppointmentController {

    private AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService){
        this.appointmentService = appointmentService;
    }

    @GetMapping(path = "/allPatients")
    public List<Appointment> getAllPatients(){
        return appointmentService.getAllPatients();
    }

    @GetMapping(path = "/appointments")
    public List<Appointment> getAppointmentsForPatient(){
        return appointmentService.getAppointmentsForPatient();
    }

    @PostMapping("/appointmentRequest")
    public void saveAppointmentRequest(@RequestBody Appointment appointment){
        appointmentService.saveAppointmentRequest(appointment);
    }

    @PatchMapping("/confirmAppointment")
    public void setAppointmentForPatient(@RequestBody Map<String, Object> data){
        appointmentService.setAppointmentForPatient(data);
    }

    @DeleteMapping(path = "/deletePatient/{id}")
    public void deletePatient(@PathVariable Long id){
        appointmentService.deletePatient(id);
    }
}
