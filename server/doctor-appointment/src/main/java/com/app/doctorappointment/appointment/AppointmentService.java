package com.app.doctorappointment.appointment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AppointmentService {

    private AppointmentRepository appointmentRepository;

    @Autowired
    public AppointmentService(AppointmentRepository appointmentRepository){
        this.appointmentRepository = appointmentRepository;
    }

    public List<Appointment> getAllPatients(){
        return appointmentRepository.findAll();
    }

    public List<Appointment> getAppointmentsForPatient(){
        return appointmentRepository.appointmentsForPatient();
    }

    public void saveAppointmentRequest(Appointment appointment){
        appointmentRepository.save(appointment);
    }

    public void setAppointmentForPatient(Map<String, Object> data){
        String objId = String.valueOf(data.get("patient_id"));
        Long id = Long.valueOf(objId);
        Optional<Appointment> opt = appointmentRepository.findById(id);
        if(opt.isPresent()) {
            Appointment a = opt.get();
            a.setDoctor((String) data.get("doctor"));
            a.setDate((String) data.get("date"));
            a.setTime((String) data.get("time"));

            String conf = String.valueOf(data.get("confirmed"));
            if(conf.equals("0")){
                a.setConfirmed(false);
            } else {
                a.setConfirmed(true);
            }
            appointmentRepository.save(a);
        }
    }

    public void deletePatient(Long id){
        appointmentRepository.deleteById(id);
    }
}
