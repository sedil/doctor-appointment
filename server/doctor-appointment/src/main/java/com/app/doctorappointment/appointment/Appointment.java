package com.app.doctorappointment.appointment;

import jakarta.persistence.*;
import org.springframework.boot.CommandLineRunner;

@Entity
@Table(name = "appointment")
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long patient_id;
    private String forname;
    private String surname;
    private String street;
    private String housenumber;
    private String postalcode;
    private String city;
    private String phonenumber;
    private String email;
    private String doctor;
    private String date;
    private String time;
    private Boolean confirmed;

    public Appointment(){}

    public Appointment(Long patient_id, String forname, String surname, String street, String housenumber, String postalcode,
                       String city, String phonenumber, String email, String doctor, String date, String time, Boolean confirmed){
        this.patient_id = patient_id;
        this.forname = forname;
        this.surname = surname;
        this.street = street;
        this.housenumber = housenumber;
        this.postalcode = postalcode;
        this.city = city;
        this.phonenumber = phonenumber;
        this.email = email;
        this.doctor = doctor;
        this.date = date;
        this.time = time;
        this.confirmed = confirmed;
    }

    public Appointment(String forname, String surname, String street, String housenumber, String postalcode,
                       String city, String phonenumber, String email, String doctor, String date, String time, Boolean confirmed) {
        this.forname = forname;
        this.surname = surname;
        this.street = street;
        this.housenumber = housenumber;
        this.postalcode = postalcode;
        this.city = city;
        this.phonenumber = phonenumber;
        this.email = email;
        this.doctor = doctor;
        this.date = date;
        this.time = time;
        this.confirmed = confirmed;
    }

    public void setId(Long patient_id){
        this.patient_id = patient_id;
    }

    @Column(name = "patient_id")
    public Long getId(){
        return this.patient_id;
    }

    public void setForname(String forname){
        this.forname = forname;
    }

    @Column(name = "forname")
    public String getForname(){
        return this.forname;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    @Column(name = "surname")
    public String getSurname(){
        return this.surname;
    }

    public void setStreet(String street){
        this.street = street;
    }

    @Column(name = "street")
    public String getStreet(){
        return this.street;
    }

    public void setHousenumber(String housenumber){
        this.housenumber = housenumber;
    }

    @Column(name = "housenumber")
    public String getHousenumber(){
        return this.housenumber;
    }

    public void setPostalcode(String postalcode){
        this.postalcode = postalcode;
    }

    @Column(name = "postalcode")
    public String getPostalcode(){
        return this.postalcode;
    }

    public void setCity(String city){
        this.city = city;
    }

    @Column(name = "city")
    public String getCity(){
        return this.city;
    }

    public void setPhonenumber(String phonenumber){
        this.phonenumber = phonenumber;
    }

    @Column(name = "phonenumber")
    public String getPhonenumber(){
        return this.phonenumber;
    }

    public void setEmail(String email){
        this.email = email;
    }

    @Column(name = "email")
    public String getEmail(){
        return this.email;
    }

    public void setDoctor(String doctor){
        this.doctor = doctor;
    }

    @Column(name = "doctor")
    public String getDoctor(){
        return this.doctor;
    }

    public void setDate(String date){
        this.date = date;
    }

    @Column(name = "date")
    public String getDate(){
        return this.date;
    }

    public void setTime(String time){
        this.time = time;
    }

    @Column(name = "time")
    public String getTime(){
        return this.time;
    }

    public void setConfirmed(Boolean confirmed){
        this.confirmed = confirmed;
    }

    @Column(name = "confirmed")
    public Boolean getConfirmed(){
        return this.confirmed;
    }

}
