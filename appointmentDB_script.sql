create database appointmentdb;
use appointmentdb;

create table appointment (
patient_id bigint unsigned auto_increment primary key,
forname varchar(32),
surname varchar(32),
street varchar(32),
housenumber varchar(8),
postalcode varchar(5),
city varchar(32),
phonenumber varchar(16),
email varchar(64),
doctor varchar(32),
date varchar(16),
time varchar(16),
confirmed bool
);

insert into appointment values (1,"Linda","Hartmann","Golberweg","2a","13441","Henshausen","+491763347761","","Dr. Thomas Anderson","04.06.2024","09:45",true);
insert into appointment values (2,"Reinhard","Tess","Hobelweg","31","26773","Energia","+49441266132","reinhard_tess@email.com","Dr. Erika Jossel","02.06.2024","13:00",true);
insert into appointment values (3,"Jasmina","Fiedler","Am Kap","4","57721","Toberberg","+493143855132","","","","",false);